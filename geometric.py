#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshk@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
import cv2
from math import sqrt

import constraints as con
import tracking

def frames():
    v = cv2.VideoCapture()
    v.open(0)
    while True:
        r, I = v.read()
        yield I

def dist(p1, p2):
    return sqrt(sum((x-y)**2 for x,y in zip(p1,p2)))

dragging = -1
def mouse_cb(event, x, y, flags, param):
    global dragging, constraints
    max_dist = 8.

    # Drag a point
    if event == cv2.EVENT_LBUTTONDOWN:
        if dragging < 0:
            d = {i: dist(known_constraints[i], (x,y)) for i in known_constraints}
            nearest = min(known_constraints.keys(), key=lambda i: d[i])
            if d[nearest] < max_dist:
                dragging = nearest
    elif event == cv2.EVENT_LBUTTONUP:
        dragging = -1
    elif event == cv2.EVENT_MOUSEMOVE:
        if dragging >= 0:
            known_constraints[dragging] = (x,y)
            if dragging in known_constraints:
                known_constraints[dragging] = (x,y,1)
    # Toggle off a point
    elif event == cv2.EVENT_MBUTTONDOWN:
        d = [dist(constraints[i], (x,y)) for i in constraints]
        nearest = min(constraints.keys(), key=lambda i: d[i])
        if d[nearest] < max_dist:
            if nearest in known_constraints:
                p = known_constraints.pop(nearest)
                if len(constrainer.get(known_constraints)) < len(constraints):
                    known_constraints[nearest] = p
                    print "ERROR: Not enough constraints to recover. Reverting"
            else:
                known_constraints[nearest] = constraints[nearest]

points = [(32,32,1), (128,32,1), (128,128,1), (32,128,1)]
constrainer = con.get_3x3()
known_constraints = dict(enumerate(points))
constraints = constrainer.get(known_constraints)

def main():
    global constrainer, known_constraints, constraints
    win = 'image'

    cv2.namedWindow(win)
    cv2.setMouseCallback(win, mouse_cb)

    horizon = False # Doesn't work in practice

    camera = frames()
    tracker = None
    comparison = None

    for index,frame in enumerate(camera):
        raw = frame.copy()
        constraints = constrainer.get(known_constraints)
        for i in range(5):
            cv2.line(frame,
                    tuple(map(int,constraints[i%4][:2])),
                    tuple(map(int,constraints[(i+1)%4][:2]))
                    , (255,0,0))
        for i,constraint in constraints.items():
            if i in known_constraints:
                color = (0,255,0)
            else:
                color = (0,0,255)
            if i < 9:
                p = int(constraint[0]/constraint[2]), int(constraint[1]/constraint[2])
                cv2.circle(frame, p, 3, color, 2)
        if tracker:
            tracked = tracker.update(raw)
            for i in tracked:
                if i > 8:
                    continue
                cv2.circle(frame, tuple(map(int, tracked[i])[:2]), 3, (200,200,0), 3)
                cv2.circle(frame, tuple(map(int, tracked[i])[:2]), 3, (0,50,50), 1)
                for i in range(5):
                    cv2.line(frame,
                            tuple(map(int,tracked[i%4][:2])),
                            tuple(map(int,tracked[(i+1)%4][:2]))
                            , (0,200,200))
        if comparison:
            tracked = comparison.update(raw)
            for i in tracked:
                if i > 8:
                    continue
                cv2.circle(frame, tuple(map(int, tracked[i])[:2]), 2, (200,0,200), 2)
        cv2.imshow(win, frame)



        key = cv2.waitKey(1)
        if key == ord('q'):
            break

        if tracker is None or key == ord('t'):
            tracked_points = {i:p for i,p in constraints.items() if i < 9}
            target_horizon = {i:p for i,p in constraints.items() if i >= 9}
            tracker = tracking.MedianRecoveryTracker(raw, tracked_points, target_horizon, constrainer)
#            tracker = tracking.RKLT(raw, tracked_points, target_horizon, constrainer)
            comparison = tracking.PointTracker(raw, tracked_points, target_horizon, constrainer)
            print 'reset tracker'

        if False and key == ord('h'):
            horizon = not horizon
            if horizon == True:
                h = con.cross(constraints[9], constraints[10], real=False)
                known_constraints[13] = con.cross(h, [0,0,1], real=False)
                known_constraints[14] = con.cross(h, [1,1,0], real=False)
            else:
                if 13 in known_constraints: known_constraints.pop(13)
                if 14 in known_constraints: known_constraints.pop(14)
                for c in range(4):
                    known_constraints[c] = constraints[c]

if __name__ == "__main__":
    main()
