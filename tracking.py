#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshk@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
import cv2
import numpy as np
import pdb
import random
from itertools import combinations

class RKLT:
    def __init__(self, frame, points, target, constraints):
        self.frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.h_points = np.array([(p[0]/p[2],p[1]/p[2],1.) for k,p in sorted(points.items())], dtype='float32')
        self.points = self.h_points[:,:2].astype('float32')
        self.source = points
        self.target = target
        self.constraints = constraints
        self.homography = np.eye(3)

        trackable = cv2.goodFeaturesToTrack(self.frame, 1000, .1, 1)
        self.trackable = np.array(filter(lambda p: test_inside(self.points, p), trackable))

        if len(self.trackable) > 5:
            print 'tracking',len(self.trackable),'/',len(trackable),'points'
        else:
            print 'tracking points'
            self.trackable = self.points
        print "trackable = "
        print trackable
        print "filtered = "
        print self.trackable

        self.trackable = self.trackable.reshape((self.trackable.shape[0], self.trackable.shape[-1]))
        self.h_trackable = np.array([(p[0],p[1],1) for p in self.trackable], dtype='float32')

        self.threshold_dist = 8.**2

    def update(self, frame):
        nextFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Track points individually
        P1, _, err = cv2.calcOpticalFlowPyrLK(self.frame, nextFrame, self.points)
        tracking, _,_ = cv2.calcOpticalFlowPyrLK(self.frame, nextFrame, self.trackable)

        # Get the intermediate homography
        X  = self.trackable
        Xp = tracking
        Hs = self.homography
        Ht,mask = cv2.findHomography(X, Xp, cv2.RANSAC)
        H = Ht.dot(Hs)

        P2 = np.array(self.h_points.dot(np.matrix(H).T))
        tracked = {i:(p[0],p[1],1) for i,p in enumerate(P1)}
        computed = {i:(p[0]/p[2],p[1]/p[2],1) for i,p in enumerate(P2)}

        saved = tracked.copy()

        def d(P, Q):
            return {i:sum(np.subtract(P[i],Q[i])**2) for i in P}

        D = d(computed, tracked)

        worst = reversed(sorted(saved.keys(), key=lambda x: D[x]))
        recovered = self.constraints.get(saved)
        while len(recovered) == self.constraints.count:
            good = recovered
            i = next(worst)
            if D[i] < self.threshold_dist:
                break
            saved.pop(i)
            recovered = self.constraints.get(saved)

        pts = np.array([(p[0],p[1]) for i,p in good.items()])

        self.homography = H
        self.frame = nextFrame
        self.points = pts[:self.constraints.count-len(self.target),:2].astype('float32')
        self.h_trackable = np.array(self.h_trackable.dot(np.matrix(H).T))
        self.source = tracked
        return good


class PointTracker:
    def __init__(self, frame, points, target, constraints):
        self.frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.points = np.array([(p[0]/p[2],p[1]/p[2]) for k,p in sorted(points.items())], dtype='float32')
        self.target = target
        self.constraints = constraints

        self.max_iterations = 20
        self.min_points = 7
        self.threshold_dist = 30.

    def update(self, frame):
        nextFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        pts, _,err = cv2.calcOpticalFlowPyrLK(self.frame, nextFrame, self.points)

        tracked = {i:(p[0],p[1],1) for i,p in enumerate(pts)}

        self.frame = nextFrame
        self.points = pts[:self.constraints.count-len(self.target),:2].astype('float32')

        return self.constraints.get(tracked)

class MedianRecoveryTracker:
    def __init__(self, frame, points, target, constraints):
        self.frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.points = np.array([(p[0]/p[2],p[1]/p[2]) for k,p in sorted(points.items())], dtype='float32')
        self.target = target
        self.constraints = constraints

        self.max_iterations = 20
        self.min_points = 7
        self.threshold_dist = 30.

    def update(self, frame):
        nextFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        pts, _,err = cv2.calcOpticalFlowPyrLK(self.frame, nextFrame, self.points)

        tracked = {i:(p[0],p[1],1) for i,p in enumerate(pts)}

        best_guess = self.median_best_guess(pts, tracked, err)

        self.frame = nextFrame
        self.points = best_guess[:self.constraints.count-len(self.target),:2].astype('float32')

        return {i:(p[0],p[1],1) for i,p in enumerate(best_guess)}

    def delete_worst(self, pts, trackers, tracked):
        D = dist(trackers, pts)
        def horizon_dist(P):
            return sum(sum( np.subtract(P[k],self.target[k])**2 for k in self.target))

        best = float('inf')
        saved = tracked.copy()
        constrained = self.constraints.get(tracked)
        deleted = 0
        for worst in sorted(D.keys(), key=lambda i: D[i], reverse=True):
            if deleted == 4:
                continue
            if D[worst] < self.threshold_dist:
                continue
            p = saved.pop(worst)
            now = self.constraints.get(saved)
            if len(now) < self.constraints.count:
                saved[worst] = p
                break
            d = horizon_dist(constrained)
            if d < best and (d-best)**2 > 100.0:
                best = d
                constrained = now
                deleted += 1
                print 'deleted',worst
            else:
                saved[worst] = p
        self.target = {i:constrained[i] for i in self.target}
        return np.array([(p[0],p[1],1) for k,p in constrained.items()])

    def median_best_guess(self, pts, tracked, err):
        constrained = [list() for i in range(self.constraints.count)]
        # Recover using good points first
        D = dist(self.points, pts)
        good_points = {i:tracked[i] for i,d in D.items() if d < self.threshold_dist}
        if len(good_points) > 4:
            for z,choice in enumerate(combinations(sorted(good_points.keys(), key=lambda x: good_points[x]), 4)):
                if z > self.max_iterations:
                    continue
                # Delete 4 points randomly
                # choice = random.sample(good_points.keys(), min(len(good_points),5))
                trial = {i:good_points[i] for i in choice}
                c = self.constraints.get(trial)
                # check if the trial was unsuccessful
                if len(c) < 9:
                    continue
                for i in c:
                    constrained[i].append(c[i])
        # Recover using any points
        while len(constrained[0]) < self.min_points:
            # Delete 4 points randomly
            choice = random.sample(tracked.keys(), 5)
            trial = {i:tracked[i] for i in choice}
            c = self.constraints.get(trial)
            # check if the trial was unsuccessful
            if len(c) < 9:
                continue
            for i in c:
                constrained[i].append(c[i])

        return np.median(constrained, 1)

def dist(P1, P2):
    return dict(enumerate(np.sum((P1-P2)**2,1)))

def wedge(o,p1,p2):
    v1 = np.subtract(p1,o)
    v2 = np.subtract(p2,o)
    return np.cross(v1[:2],v2[:2])

def test_inside(polygon, p):
    s = wedge(polygon[-1,:],polygon[0,:],p) > 0
    vp = polygon[0,:]
    for i in range(1,len(polygon)):
        v = polygon[i,:]
        if (wedge(vp,v,p)>0) != s:
            return False
        vp = v
    return True

