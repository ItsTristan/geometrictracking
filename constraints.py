#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tristan Meleshko <tmeleshk@ualberta.ca>
#
# Distributed under terms of the MIT license.

"""

"""
class Constraints:
    def __init__(self, count):
        self.count = count
        self.constraints = {}
        self._recovery = {}

    def __getitem__(self, i):
        return self.constraints.get(i, None)

    def add_recovery(self, i, P1,P2,P3,P4):
        """
        Adds a method to recover point i using
        the set of points P1, P2, P3, P4.
        If multiple points can be used, you can
        use a list. Otherwise, use an integer
        """
        if isinstance(P1, int):
            P1 = [P1]
        if isinstance(P2, int):
            P2 = [P2]
        if isinstance(P3, int):
            P3 = [P3]
        if isinstance(P4, int):
            P4 = [P4]
        self._recovery[i] = self._recovery.get(i, set())
        for p1 in P1:
            for p2 in P2:
                for p3 in P3:
                    for p4 in P4:
                        if len(set([p1,p2,p3,p4,i])) == 5:
                            self._recovery[i].add( (p1,p2,p3,p4) )
                            print 'Added recovery for {}: {}'.format(i, (p1,p2,p3,p4))

    def get_recovery(self, i, known):
        """
        Returns a set of points that can reconstruct i, or None otherwise
        """
        for recovery in self._recovery.get(i, []):
            if all(r in known for r in recovery):
                return recovery
        return None

    def recover(self, i, known, verbose=False):
        """
        Returns the point i given the set of known points,
        or None if not possible.
        """
        if i in known:
            return known[i]
        recovery = self.get_recovery(i, known)
        if recovery is None:
            return None
        if verbose:
            print 'Found recovery for {}: {}'.format(i, recovery)
        return join(*[known[i] for i in recovery])

    def get(self, known_points):
        """
        Returns all recoverable constraints
        known is a dictionary mapping indexes to homogenous coordinates
        """
        known = dict(known_points.items())
        unknown = set(xrange(self.count)).difference(known.keys())
        remaining = len(unknown)
        prev_remaining = -1
        while unknown and remaining != prev_remaining:
            prev_remaining = remaining
            for i in list(unknown):
                p = self.recover(i, known)
                if p is not None:
                    known[i] = p
                    unknown.remove(i)
            remaining = len(unknown)
        return known

def cross(p1, p2, real=True):
    a,b,c = p1
    d,e,f = p2
    if real:
        return (b*f-c*e).real, (c*d-a*f).real, (a*e-b*d).real
    else:
        return b*f-c*e, c*d-a*f, a*e-b*d

def join(p1,p2,p3,p4):
    a = normalize(cross(p1,p2))
    b = normalize(cross(p3,p4))
    return normalize(cross(a,b))

def normalize(p):
    if p[2] != 0:
        return float(p[0])/p[2], float(p[1])/p[2], 1
    else:
        return p

def get_3x3():
    """
                                        12

                0   4   1

                7   8   5           9

                3   6   2

                            11

                    10
    """

    C = Constraints(13)
    # Get edges
    E = []
    E.append(set([0,1,4,9]))
    E.append(set([1,5,2,10]))
    E.append(set([3,6,2,9]))
    E.append(set([0,7,3,10]))
    E.append(set([0,8,2]))
    E.append(set([1,8,3]))
    E.append(set([4,6,8,10]))
    E.append(set([7,8,5,9]))
    E.append(set([4,5,11]))
    E.append(set([7,6,11]))
    E.append(set([0,2,8,11]))
    E.append(set([4,7,12]))
    E.append(set([5,6,12]))
    E.append(set([1,3,8,12]))
    E.append(set([9,10,11,12]))

    # Add recovery options
    for i in xrange(len(E)):
        E1 = E[i]
        for j in xrange(i+1, len(E)):
            E2 = E[j]
            for k in E1.intersection(E2):
                C.add_recovery(k, E1,E1,E2,E2)

    count = 0
    for i in xrange(13):
        count += len(C._recovery[i])

    print 'Generated {} constraints'.format(count)
    return C

def get_5x5():
    """
    """

    C = Constraints(13)
    # Get edges
    E = []
    E.append(set([0,1,4,9]))
    E.append(set([1,5,2,10]))
    E.append(set([3,6,2,9]))
    E.append(set([0,7,3,10]))
    E.append(set([0,8,2]))
    E.append(set([1,8,3]))
    E.append(set([4,6,8,10]))
    E.append(set([7,8,5,9]))
    E.append(set([4,5,11]))
    E.append(set([7,6,11]))
    E.append(set([0,2,8,11]))
    E.append(set([4,7,12]))
    E.append(set([5,6,12]))
    E.append(set([1,3,8,12]))
    E.append(set([9,10,11,12]))

    # Add recovery options
    for i in xrange(len(E)):
        E1 = E[i]
        for j in xrange(i+1, len(E)):
            E2 = E[j]
            for k in E1.intersection(E2):
                C.add_recovery(k, E1,E1,E2,E2)

    count = 0
    for i in xrange(13):
        count += len(C._recovery[i])

    print 'Generated {} constraints'.format(count)
    return C




